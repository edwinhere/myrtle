-- Graceful Labeling of trees
--
-- This module should implement a function which produces a graceful labeling
-- of a given tree
--
-- The function 'gracefulLabeling' should be implemented such that it passes the
-- QuickCheck property in 'prop_graceful'.
--
-- See https://en.wikipedia.org/wiki/Graceful_labeling for some more
-- information on graceful labeling.
--
-- Feel free to make whatever changes to the code you need to, as long as the
-- spirit of the task remains the same.
--
-- The speed of the code doesn't matter hugely, the clarity is more important.

module Graceful where

import           Data.List       (permutations, sort)
import           Data.List.Split
import           Data.Monoid     (All (..), (<>))
import           System.Random
import           Test.QuickCheck

-- | @Tree v e@ is a tree with the vertices labeled with @v@s and the edges
-- labeled with @e@s.
data Tree v e = Tree v [(e, Tree v e)]
  deriving(Show, Eq)

-- | 'gracefulLabeling' is a function which labels the vertices and edges of a
-- tree with m vertices such that:
--
-- - Every vertex has a unique integral value in the range [0..m]
-- - Every edge, (v1, v2), has a unique integral value equal to the absolute
-- difference between v1 an v2.
--
-- @isGraceful . gracefulLabeling@ should be True for all possible input
-- 'Tree's and the output tree should be the same shape as the input tree.
gracefulLabeling :: (Show a, Show b) => Tree a b -> Tree Int Int
gracefulLabeling t = firstSolution
  where
    m :: Int
    m = length (vertices t) - 1
    range :: [Int]
    range = [0..m]
    mutations :: [[Int]]
    mutations = permutations range
    label :: [Int] -> Tree Int Int
    label is = labelTree is t
    allLabellings :: [Tree Int Int]
    allLabellings = fmap label mutations
    gracefulLabellings :: [Tree Int Int]
    gracefulLabellings = filter isGraceful allLabellings
    firstSolution :: Tree Int Int
    firstSolution = head gracefulLabellings

labelTree :: [Int] -> Tree a b -> Tree Int Int
labelTree (l:ls) (Tree _ bs) = Tree l rest
  where
    choppy = zip bs (splitPlaces (map (length . vertices . snd) bs) ls)
    rest = map g choppy
    g ((_, t), ls') = construct (labelTree ls' t)
    construct x@(Tree l' _) = (abs (l - l'), x)

--------------------------------------------------------------------------------
-- Some utility functions for working with the 'Tree' type
--------------------------------------------------------------------------------

-- Given a consumer for vertices and a consumes for edges reduce the tree.
foldTree :: Monoid m => (v -> m -> a) -> (e -> a -> m) -> Tree v e -> a
foldTree f g (Tree v es) = f v (foldMap (\(e, t) -> g e (foldTree f g t)) es)

-- | Get the vertices in a 'Tree'
vertices :: Tree v e -> [v]
vertices = foldTree (:) (const id)

-- | Get the edges in a 'Tree'
edges :: Tree v e -> [e]
edges = foldTree (const id) (:)

-- | This 'Arbitrary' instance generates sized trees where the size represents
-- the number of vertices.
instance (Arbitrary v, Arbitrary e) => Arbitrary (Tree v e) where
  arbitrary = sized $ \n ->
    if n == 0
      then Tree <$> arbitrary <*> pure []
      else do
        let n' = pred n
        ns <- sumList n'
        v <- arbitrary
        es <- traverse (`resize` arbitrary) ns
        pure $ Tree v es
    where
      -- @sumList n@ returns a list where the sum of the elements is @n@ and no
      -- element is zero.
      sumList :: (Integral a, Random a) => a -> Gen [a]
      sumList n = case n of
        0 -> pure []
        _ -> do
          i <- choose (1, n)
          (i:) <$> sumList (n - i)
  shrink (Tree v es) = Tree <$> shrink v <*> shrink es

--------------------------------------------------------------------------------
-- QuickCheck property and Arbitrary instance
--------------------------------------------------------------------------------

-- | Test the 'gracefulLabeling' function on trees with up to 20 vertices.
test :: IO ()
test = quickCheckWith stdArgs{maxSize = 20} prop_graceful

-- | Does 'gracefulLabeling' do the correct thing for the given tree.
prop_graceful :: Tree () () -> Bool
prop_graceful t = let g = gracefulLabeling t
                  in isSameShape t g && isGraceful g

-- | @isGraceful t@ returns 'True' iff @t@ is gracefully labeled.
isGraceful :: Tree Int Int -> Bool
isGraceful t =
  let vs = vertices t
      es = edges t
      okValues = getAll . snd . foldTree
        (\v m -> (v, m v))
        -- ^ return (the current vertex, if the edges are OK)
        (\e (v2, b) v1 -> All (e == abs (v1 - v2)) <> b)
        -- ^ return a function to check if this edge is ok when given the
        -- parent vertex as well as the subtree
      inRange x = x >= 0 && x < length vs
      anySame l = or (zipWith (==) s (tail s))
        where s = sort l
  in not (anySame vs)
  && not (anySame es)
  && all inRange vs
  && okValues t

isSameShape :: Tree a b -> Tree c d -> Bool
isSameShape t1 t2 = toUnit t1 == toUnit t2
  where -- Replace the vertex and edge labels with ()
        toUnit = foldTree (const (Tree ()))
                          (\_ t -> [((), t)])
