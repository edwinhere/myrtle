{-# LANGUAGE DeriveFunctor     #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE PatternSynonyms   #-}
{-# LANGUAGE TemplateHaskell   #-}

-- | This modules describes a simplifies syntax tree for a language supporting
-- addition, subtraction and multiplication.
--
-- It also has functionality for evaluating the function represented by an AST
module AST
  ( -- * The AST
    ASTF(..)
  , AST
  , mapAST
  , eval
  , evalA
  , pattern Add
  , pattern Sub
  , pattern Mul
  , pattern Sym
    -- * Streams
  , StreamName(..)
  ) where

import           Data.Functor.Foldable
import           Data.Functor.Identity
import           Test.QuickCheck
import           Text.Show.Deriving

-- | An AST representing a simple expression language. The AST is the fixed
-- point of the functor @AST a@. The leaves of the AST are of type @a@
data ASTF a f = AddF f f
              | SubF f f
              | MulF f f
              | SymF a
  deriving(Functor, Show)

-- Remove this if not using GHC 8
$(deriveShow1 ''ASTF)

-- | A handy type synonym to avoid using the 'Fix' type constructor everywhere
type AST a = Fix (ASTF a)

-- | Some useful patterns to avoid using 'Fix' everywhere
infixl 7 `Mul`
infixl 6 `Add`, `Sub`
pattern Add x y = Fix (AddF x y)
pattern Sub x y = Fix (SubF x y)
pattern Mul x y = Fix (MulF x y)
pattern Sym n   = Fix (SymF n)

--------------------------------------------------------------------------------
-- Some utilities for working with ASTs
--------------------------------------------------------------------------------

-- | Transform the values in the `SymF` constructors of an AST
mapAST :: (a -> b) -> AST a -> AST b
mapAST f = ana $ \case
                    Add x y -> AddF x y
                    Sub x y -> SubF x y
                    Mul x y -> MulF x y
                    Sym s   -> SymF (f s)

-- | Shorthand for 'evalA' over Identity
eval :: Num a => AST a -> a
eval = runIdentity . evalA . mapAST Identity

-- | Evaluate an AST
evalA :: (Applicative t, Num a) => AST (t a) -> t a
evalA = cata $ \case
                  SymF i -> i
                  AddF x y -> (+) <$> x <*> y
                  SubF x y -> (-) <$> x <*> y
                  MulF x y -> (*) <$> x <*> y

--------------------------------------------------------------------------------
-- Symbolic stream names
--------------------------------------------------------------------------------

-- | A StreamName represents one of 26 streams, 'a' .. 'z'
newtype StreamName = StreamName Char
  deriving(Show)

--------------------------------------------------------------------------------
-- QuickCheck arbitrary instances
--------------------------------------------------------------------------------

instance Arbitrary a => Arbitrary (Fix (ASTF a)) where
  arbitrary = sized tree'
    where -- @tree' n@ Generates an AST of at most depth @n@
          tree' :: Arbitrary a => Int -> Gen (AST a)
          tree' n = let subTree = tree' (pred n)
                    in if n == 0
                      then Sym <$> arbitrary
                      else oneof [ Sym <$> arbitrary
                                 , Add <$> subTree <*> subTree
                                 , Sub <$> subTree <*> subTree
                                 , Mul <$> subTree <*> subTree
                                 ]

instance Arbitrary StreamName where
  arbitrary = StreamName <$> choose ('a', 'z')

instance CoArbitrary StreamName where
  coarbitrary (StreamName c) = coarbitrary c
