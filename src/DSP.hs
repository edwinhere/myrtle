{-# LANGUAGE DeriveFunctor              #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

-- | This module defines a data type for representing complex combinations of
-- signals.
module DSP
  ( DSPGraph(..)
  , SignalIndex(..)
  , DSPVertex(..)
  , eval
  , dspLatency
  , exampleGraph
  ) where

import           Control.Applicative
import           Data.Array

import           Signal

-- | A 'DSPGraph' contains a mapping from 'SignalIndex's to 'DSPVertex's. Each
-- 'DSPVertex' in this table should refer to other 'DSPVertex's in the table by
-- their 'SignalIndex'. This representation is chosed over a tree to facilitate
-- potential sharing of signals. For example the AST @x * y + x * y@ could be
-- transformed into: @DSPGraph 0 [(0, Add 1 1), (1, Mul 2 3), (2, Sig x), (3, Sig y)]@
--
-- The lone 'SignalIndex' is the pointer to the final output of the DAG.
data DSPGraph = DSPGraph SignalIndex [(SignalIndex, DSPVertex SignalIndex)]

-- | An index into a group of signals
newtype SignalIndex = SignalIndex Word
  deriving (Eq, Ord, Ix)

-- | The operations which can be performed on signals.
data DSPVertex a = Reg Word a
                   -- ^ @n@ registers, these delay the signal @a@ by @n@ cycles
                 | Add a a
                   -- ^ The addition of two signals
                 | Neg a
                   -- ^ The negation of a signal
                 | Mul a a
                   -- ^ The multiplication of two signals
                 | Mad a a a
                   -- ^ The multiplication of two signals followed by the
                   -- addition of a third. The value @Mad a b c@ represents the
                   -- value @(a * b) + c@
                 | Sig (Signal Int)
                   -- ^ An input signal, these form the leaves of the DAG or
                   -- Tree
  deriving Functor

-- | An example graph representing the DAG:
--
--         out
--          |
--        Reg 4
--          |
--         Mul
--        /   \
--     Reg 5  Add
--        \   /  \
--       pure 2  pure 1
--
-- This could have come from the AST perhaps
--
--         out
--          |
--         Mul
--         / \
--     pure 2 \
--           Add
--           /  \
--      pure 1  pure 2
exampleGraph :: DSPGraph
exampleGraph = DSPGraph (SignalIndex 4)
  [ (SignalIndex 1, Sig (pure 1))
  , (SignalIndex 2, Sig (pure 2))
  , (SignalIndex 3, Add (SignalIndex 1) (SignalIndex 2))
  , (SignalIndex 4, Mul (SignalIndex 6) (SignalIndex 3))
  , (SignalIndex 5, Reg 3 (SignalIndex 4))
  , (SignalIndex 6, Reg 5 (SignalIndex 2))
  ]

-- | Generate the infinite stream of values represented by a DSPGraph.
eval :: DSPGraph -> Signal Int
eval (DSPGraph top graph) = sigs ! top
  where leastIndex = minimum (fst <$> graph)
        greatestIndex = maximum (fst <$> graph)

        -- sigs is a fixed point over mapping the vertices to signals
        sigs = vertexToSignal <$> array (leastIndex, greatestIndex) graph

        vertexToSignal :: DSPVertex SignalIndex -> Signal Int
        vertexToSignal v = case (sigs !) <$> v of
          Reg d i   -> delay d i
          Add a b   -> add a b
          Neg a     -> neg a
          Mul a b   -> mul a b
          Mad a b c -> mad a b c
          Sig s     -> s


-- The number of cycles a value takes to propagate through any of the Add, Neg,
-- Mul or Mad vertices. Note that this doesn't affect the throughput, 5 values
-- are 'in flight' at any moment.
dspLatency :: Word
dspLatency = 5

--------------------------------------------------------------------------------
-- Some helpers for eval
--------------------------------------------------------------------------------

(+.) :: (Applicative f, Num a) => f a -> f a -> f a
(+.) = liftA2 (+)
infixl 6 +.

(*.) :: (Applicative f, Num a) => f a -> f a -> f a
(*.) = liftA2 (*)
infixl 7 *.

add :: Signal Int -> Signal Int -> Signal Int
add a b = delay dspLatency (a +. b)

neg :: Signal Int -> Signal Int
neg = delay dspLatency . fmap negate

mul :: Signal Int -> Signal Int -> Signal Int
mul a b = delay dspLatency (a *. b)

mad :: Signal Int -> Signal Int -> Signal Int -> Signal Int
mad a b c = delay dspLatency (a *. b +. c)

