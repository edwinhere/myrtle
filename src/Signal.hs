{-# LANGUAGE BinaryLiterals  #-}
{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE DeriveFunctor   #-}
{-# LANGUAGE LambdaCase      #-}
{-# LANGUAGE PatternSynonyms #-}
{-# OPTIONS_GHC -fno-warn-unticked-promoted-constructors #-}

-- | This module defines a data type for representing changing signals in a
-- circuit.
--
-- Some transformers for signals are defined in the DSP module.
--
module Signal
  ( -- * Signals
    Signal
  , toList
  , delay
  ) where

import           Test.QuickCheck

--------------------------------------------------------------------------------
-- Signal functionality
--------------------------------------------------------------------------------

-- | An infinite list of values. Representing the changing value of a wire in a
-- circuit over time.
data Signal a = a :- Signal a
  deriving Functor

infixr 5 :-

-- | The 'Applicative' instance for 'Signal' is very similar to the
-- 'Applicative' instance for 'ZipList'
instance Applicative Signal where
  pure x = x :- pure x
  (f :- fs) <*> (x :- xs) = f x :- (fs <*> xs)

instance Arbitrary a => Arbitrary (Signal a) where
  arbitrary = fromList <$> infiniteList

-- | Show the first 10 values of a signal.
instance Show a => Show (Signal a) where
  showsPrec n = showsPrec n . take 10 . toList

-- | Convert a 'Signal' to an infinite list.
toList :: Signal a -> [a]
toList (a :- as) = a : toList as

-- | Convert a list to a Signal, this list must be infinite
fromList :: [a] -> Signal a
fromList = \case
  (x:xs) -> x :- fromList xs
  []     -> error "fromList: Finite list"

-- | @delay n@ delays the signals on a wire by @n@ cycles by inserting @n@
-- undefined elements at the beginning.
delay :: Word -> Signal Int -> Signal Int
delay w s = foldr (:-) s d
  where d = replicate (fromIntegral w) err
        err = error "register initial value"

