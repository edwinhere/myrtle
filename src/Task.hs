{-# LANGUAGE LambdaCase #-}
module Task where

import           Data.Functor.Foldable
import           Test.QuickCheck

import           AST
import           DSP
import           Signal

-- | ast2dsp takes as input a mapping from 'StreamName's to 'Signals' and an
-- 'AST' with 'StreamName's at the leaves. It should produce a valid 'DSPGraph'
-- along with the total latency of the graph, i.e. the time it takes for the
-- first output to become valid.o
--
-- It's important to insert the appropriate number of delaying elements (Reg)
-- before each of Add, Neg, Mul and Mad to ensure that the correct inputs arrive
-- at the right time.
--
-- Take for example the AST: (a + b) + c
-- Assume 'dspLatency' is 5
--
-- The following DAG is incorrect, the addition between a and b, Add1,
-- introduces a latency of 5 cycles. The path from each leaf to the top of the
-- tree must have the same latency
--
--      Add2
--     /    \
--    Add1   \
--   /    \   \
--  a      b   c
--
-- This is the correct DAG, the c input has been delayed by Reg 5 so the c input
-- arrives at Add2 at the same time as the result of Add1. The total latency of
-- this graph is 10, the cumulative delay from the top vertex to each of the
-- leaves.
--
--      Add2
--     /    \
--    Add1  Reg 5
--   /    \   \
--  a      b   c
--
--
-- There are several optimizations that ast2dsp can do. The AST @(a * b) + c@
-- can be represented by a single Mad (Multiply ADd) vertex. It's also possible
-- to share work if subtrees represent equivalent expressions.
ast2dsp :: (StreamName -> Signal Int) -> AST StreamName -> (DSPGraph, Word)
ast2dsp f = compile . mapAST (toGraph . f)
  where
    toGraph :: Signal Int -> (DSPGraph, Word)
    toGraph s = (DSPGraph (SignalIndex 0) [(SignalIndex 0, Sig s)], 0)

compile :: AST (DSPGraph, Word) -> (DSPGraph, Word)
compile = cata $ \case
                      SymF graph -> graph
                      AddF x y -> merge x y (DSP.Add undefined undefined)
                      MulF x y -> merge x y (DSP.Mul undefined undefined)
                      SubF x y -> merge x (neg y) (DSP.Add undefined undefined)

merge :: (DSPGraph, Word) -> (DSPGraph, Word) -> DSPVertex SignalIndex -> (DSPGraph, Word)
merge x@(DSPGraph xroot xgraph, xdelay) y@(DSPGraph yroot ygraph, ydelay) z = let
  (delayedX@(DSPGraph delayedXRoot delayedXGraph), newXDelay) = if xdelay < ydelay
    then reg (ydelay - xdelay) x
    else x
  (delayedY@(DSPGraph delayedYRoot delayedYGraph), newYDelay) = if ydelay < xdelay
    then reg (xdelay - ydelay) y
    else y
  offset = fromIntegral $ length delayedXGraph
  shiftedY@(DSPGraph shiftedYRoot shiftedYGraph) = shift delayedY offset
  newRoot = inc shiftedYRoot
  xy = delayedXGraph ++ shiftedYGraph
  xyz = case z of
    DSP.Add _ _ -> xy ++ [(newRoot, DSP.Add delayedXRoot shiftedYRoot)]
    DSP.Mul _ _ -> xy ++ [(newRoot, DSP.Mul delayedXRoot shiftedYRoot)]
  in (DSPGraph newRoot xyz, newYDelay + dspLatency)

neg :: (DSPGraph, Word) -> (DSPGraph, Word)
neg (DSPGraph root graph, d) = (DSPGraph (inc root) (graph ++ [(inc root, DSP.Neg root)]), d + dspLatency)

reg :: Word -> (DSPGraph, Word) -> (DSPGraph, Word)
reg n (DSPGraph root graph, d) = (DSPGraph (inc root) (graph ++ [(inc root, DSP.Reg n root)]), d + n)

inc :: SignalIndex -> SignalIndex
inc (SignalIndex w) = SignalIndex (w + 1)

add :: Word -> SignalIndex -> SignalIndex
add x (SignalIndex w) = SignalIndex $ x + w

shift :: DSPGraph -> Word -> DSPGraph
shift (DSPGraph root graph) offset = DSPGraph (add offset root) shiftedGraph
  where
    shiftedGraph :: [(SignalIndex, DSPVertex SignalIndex)]
    shiftedGraph = fmap f graph
    f :: (SignalIndex, DSPVertex SignalIndex) -> (SignalIndex, DSPVertex SignalIndex)
    f (i, DSP.Add x y) = (add offset i, DSP.Add (add offset x) (add offset y))
    f (i, DSP.Mul x y) = (add offset i, DSP.Mul (add offset x) (add offset y))
    f (i, DSP.Reg n y) = (add offset i, DSP.Reg n (add offset y))
    f (i, DSP.Neg y) = (add offset i, DSP.Neg (add offset y))
    f (i, DSP.Mad x y z) = (add offset i, DSP.Mad (add offset x) (add offset y) (add offset z))
    f (i, DSP.Sig s) = (add offset i, DSP.Sig s)

--------------------------------------------------------------------------------
-- QuickCheck property
--------------------------------------------------------------------------------

-- | The number of elements to check for correctness
numToCheck :: Int
numToCheck = 100

-- | Compare the first 'numToCheck' values of the evaluation of the 'AST' with the
-- first 'numToCheck' valid values of evaluation of the 'DSPTree'.
prop_eval :: Blind (StreamName -> Signal Int) -> AST StreamName -> Property
prop_eval (Blind f) t =
  let (dspTree, latency) = ast2dsp f t
  in (take numToCheck . toList $ AST.evalA (mapAST f t))
 === (take numToCheck . drop (fromIntegral latency) . toList $ DSP.eval dspTree)

test :: IO ()
test = quickCheckWith stdArgs{maxSize = 20} prop_eval
